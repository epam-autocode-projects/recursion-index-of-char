﻿using System;
using System.Text;

namespace RecursionIndexOfChar
{
    public static class GetIndexRecursively
    {
        private static int k = 0;

        public static int GetIndexOfChar(string str, char value)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return -1;
            }

            if (str.Length - 1 == 51)
            {
                k = 0;
            }

            if (str[0] == value)
            {
                return k;
            }

            k++;

            return GetIndexOfChar(str[1..], value);
        }

        public static int GetIndexOfChar(string str, char value, int startIndex, int count)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return -1;
            }

            if (str.Length - 1 == 51)
            {
                k = startIndex;
                str = str[startIndex ..];
            }

            if (count <= 0)
            {
                return -1;
            }

            if (str[0] == value)
            {
                return k;
            }

            k++;

            return GetIndexOfChar(str[1..], value, startIndex, count - 1);
        }
    }
}
